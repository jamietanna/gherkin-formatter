# gherkin-formatter

A Java utility to convert from a parsed Gherkin (feature) file to a pretty-printed string.

## Usage

### Dependency

This library can be found at the below coordinates:

```xml
<dependency>
    <groupId>me.jvt.cucumber</groupId>
    <artifactId>gherkin-formatter</artifactId>
    <version>$version</version>
</dependency>
```

```groovy
implementation 'me.jvt.cucumber:gherkin-formatter:$version'
```

Where the versions can be found [in Maven Central](https://mvnrepository.com/artifact/me.jvt.cucumber/gherkin-formatter).

### Example usage

```java
// initialise once
PrettyFormatter formatter;
// to use with default indentation
formatter = new PrettyFormatter();
// to use with custom indentation
formatter = new PrettyFormatter(10);

// then, to format a feature file:
String feature = "Feature: ...\n\nScenario: ...\n";
String pretty = formatter.format(feature);
```
