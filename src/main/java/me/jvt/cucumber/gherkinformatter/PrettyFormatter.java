package me.jvt.cucumber.gherkinformatter;

import io.cucumber.gherkin.Gherkin;
import io.cucumber.messages.IdGenerator;
import io.cucumber.messages.types.*;
import java.util.*;
import java.util.stream.Collectors;
import org.apache.commons.text.WordUtils;

/** A Java utility to pretty-print a Gherkin feature file. */
public class PrettyFormatter {
  private static final int LINE_LENGTH = 120;
  private final int indentationInSpaces;

  static class State {
    private final StringJoiner joiner;
    private final GherkinDocument document;
    private final List<Comment> comments;

    State(StringJoiner joiner, GherkinDocument document) {
      this.joiner = joiner;
      this.document = document;
      this.comments = new ArrayList<>(this.document.getComments());
    }

    public StringJoiner getJoiner() {
      return joiner;
    }

    public GherkinDocument getDocument() {
      return document;
    }

    public List<Comment> getComments() {
      return comments;
    }
  }

  /** Construct a new {@link PrettyFormatter}, with default indentation. */
  public PrettyFormatter() {
    indentationInSpaces = 2;
  }

  /**
   * Construct a new {@link PrettyFormatter}, with specific indentation.
   *
   * @param indentationInSpaces the number of spaces to indent with
   */
  public PrettyFormatter(int indentationInSpaces) {
    this.indentationInSpaces = indentationInSpaces;
  }

  /**
   * Pretty-print a given Gherkin feature file.
   *
   * @param src the Gherkin feature file, as a String.
   * @return the pretty-printed feature file
   * @throws CouldNotParseException if the feature file could not be parsed
   */
  public String format(String src) {
    List<Envelope> envelopes =
        Gherkin.fromSources(
                Collections.singletonList(Gherkin.makeSourceEnvelope(src, "stdin")),
                false,
                true,
                false,
                new IdGenerator.Incrementing())
            .collect(Collectors.toList());

    return format(envelopes);
  }

  /**
   * Pretty-print a given Gherkin feature file.
   *
   * @param envelopes the Gherkin feature file, as a <code>List</code> of {@link Envelope}s. The 0th
   *     element in the List must contain a {@link GherkinDocument}.
   * @return the pretty-printed feature file
   * @throws CouldNotParseException if the feature file could not be parsed
   */
  public String format(List<Envelope> envelopes) {
    if (null == envelopes) {
      throw new IllegalArgumentException("The `envelopes` was null");
    }
    if (envelopes.isEmpty()) {
      throw new IllegalArgumentException("The `envelopes` was empty");
    }

    if (envelopes.get(0).getParseError() != null) {
      throw new CouldNotParseException(envelopes.get(0).getParseError());
    }
    if (envelopes.get(0).getGherkinDocument() == null) {
      throw new IllegalArgumentException(
          "The `envelopes` must contain a `GherkinDocument` in the 0th element");
    }

    State state = new State(new StringJoiner("\n"), envelopes.get(0).getGherkinDocument());
    addFeature(state, 0, state.getDocument().getFeature());

    if (!state.getComments().isEmpty()) {
      for (Comment c : new ArrayList<>(state.getComments())) {
        addComment(state, 0, c);
      }
    }

    return transformFormatted(state.getJoiner().toString());
  }

  private String indent(int indentLevel, String text) {
    StringBuilder indent = new StringBuilder();
    for (int i = 0; i < indentationInSpaces; i++) {
      indent.append(" ");
    }

    StringBuilder s = new StringBuilder();
    for (int i = 0; i < indentLevel; i++) {
      s.append(indent);
    }
    return s + text;
  }

  private void addCommentsIfRequired(State state, int indent, Location current) {
    List<Comment> comments =
        state.getComments().stream()
            .filter(c -> c.getLocation().getLine() <= current.getLine())
            .collect(Collectors.toList());

    for (Comment c : comments) {
      addComment(state, indent, c);
    }
  }

  private void addComment(State state, int indent, Comment comment) {
    // remove it so we don't re-iterate it
    state.getComments().remove(comment);
    state.getJoiner().add(indent(indent, ltrim(comment.getText())));
  }

  private void addFeature(State state, int indent, Feature feature) {
    if (feature == null) {
      return;
    }

    addCommentsIfRequired(state, indent, feature.getLocation());

    if (!"en".equals(feature.getLanguage())) {
      state.getJoiner().add(String.format("# language: %s", feature.getLanguage()));
    }

    addTags(state, indent, feature.getTags());
    addSectionHeader(state, indent, feature);
    // in a feature we want to indent, but not elsewhere
    addDescription(state, indent + 1, feature.getDescription());

    state.getJoiner().add("");

    List<FeatureChild> children = feature.getChildren();
    for (FeatureChild ch : children) {
      addFeatureChild(state, indent + 1, ch);
      state.getJoiner().add("");
    }
  }

  private void addFeatureChild(State state, int indent, FeatureChild child) {
    addBackground(state, indent, child.getBackground());
    addScenario(state, indent, child.getScenario());
    addRule(state, indent, child.getRule());
  }

  private void addRule(State state, int indent, Rule rule) {
    if (rule == null) {
      return;
    }
    addCommentsIfRequired(state, indent, rule.getLocation());

    addTags(state, indent, rule.getTags());
    addSectionHeader(state, indent, rule);
    addDescription(state, indent, rule.getDescription());
    state.getJoiner().add("");
    for (RuleChild child : rule.getChildren()) {
      addRuleChild(state, indent + 1, child);
      state.getJoiner().add("");
    }
  }

  private void addRuleChild(State state, int indent, RuleChild child) {
    if (child == null) {
      return;
    }

    addBackground(state, indent, child.getBackground());
    addScenario(state, indent, child.getScenario());
  }

  private void addTags(State state, int indent, List<Tag> tags) {
    if (tags == null || tags.isEmpty()) {
      return;
    }
    String tagString = tags.stream().map(Tag::getName).collect(Collectors.joining(" "));

    List<String> lines = Arrays.asList(WordUtils.wrap(tagString, LINE_LENGTH).split("\n"));
    lines.forEach(l -> state.getJoiner().add(indent(indent, l)));
  }

  private void addScenario(State state, int indent, Scenario scenario) {
    if (scenario == null) {
      return;
    }
    addCommentsIfRequired(state, indent, scenario.getLocation());

    addTags(state, indent, scenario.getTags());
    addSectionHeader(state, indent, scenario);
    addDescription(state, indent, scenario.getDescription());
    addSteps(state, indent + 1, scenario.getSteps());
    addExamples(state, indent + 1, scenario.getExamples());
  }

  private void addExamples(State state, int indent, List<Examples> examples) {
    if (examples == null) {
      return;
    }

    for (Examples example : examples) {
      addExample(state, indent, example);
    }
  }

  private void addExample(State state, int indent, Examples example) {
    if (example == null) {
      return;
    }

    state.getJoiner().add("");
    addTags(state, indent, example.getTags());
    addSectionHeader(state, indent, example);
    addDescription(state, indent, example.getDescription());
    DataTable table = fromExamples(example);

    addDataTable(state, indent, table);
  }

  private void addBackground(State state, int indent, Background background) {
    if (background == null) {
      return;
    }

    addCommentsIfRequired(state, indent, background.getLocation());

    addSectionHeader(state, indent, background);
    addDescription(state, indent, background.getDescription());
    addSteps(state, indent + 1, background.getSteps());
  }

  private void addDescription(State state, int indent, String description) {
    if (description == null || description.isEmpty()) {
      return;
    }

    List<String> lines = Arrays.asList(description.split("\n"));
    lines.forEach(l -> state.getJoiner().add(indent(indent, ltrim(l))));
  }

  private static String ltrim(String l) {
    return l.replaceFirst("^\\s+", "");
  }

  private void addSteps(State state, int indent, List<Step> steps) {
    if (steps == null) {
      return;
    }

    for (Step step : steps) {
      addStep(state, indent, step);
    }
  }

  private void addStep(State state, int indent, Step step) {
    addCommentsIfRequired(state, indent, step.getLocation());
    state.getJoiner().add(indent(indent, String.format("%s%s", step.getKeyword(), step.getText())));
    addDocString(state, indent + 1, step.getDocString());
    if (step.getDataTable() != null) {
      addDataTable(state, indent, step.getDataTable());
    }
  }

  private void addDocString(State state, int indent, DocString docString) {
    if (docString == null) {
      return;
    }
    String[] lines = docString.getContent().split("\n");

    StringBuilder header = new StringBuilder();
    header.append(docString.getDelimiter());
    if (docString.getMediaType() != null) {
      header.append(docString.getMediaType());
    }
    state.getJoiner().add(indent(indent, header.toString()));
    for (String line : lines) {
      if (line.isEmpty()) {
        state.getJoiner().add("");
        continue;
      }
      if (docString.getDelimiter().equals(line)) {
        line = line.replaceAll("(.)", "\\\\$1");
      }

      state.getJoiner().add(indent(indent, line));
    }
    state.getJoiner().add(indent(indent, docString.getDelimiter()));
  }

  private void addDataTable(State state, int indent, DataTable table) {
    List<Integer> columnPadding = getMaximumLength(table.getRows());
    addCommentsIfRequired(state, indent, table.getLocation());

    for (TableRow row : table.getRows()) {
      if (row == null) {
        continue;
      }

      int innerIndent = indent + 1;
      StringJoiner tableJoiner = new StringJoiner("|");
      for (int i = 0; i < row.getCells().size(); i++) {
        TableCell cell = row.getCells().get(i);
        addCommentsIfRequired(state, innerIndent, cell.getLocation());
        tableJoiner.add(
            String.format(
                " %s ",
                StringPadder.rightPad(cellEscaping(cell.getValue()), columnPadding.get(i))));
      }
      state.getJoiner().add(indent(innerIndent, String.format("|%s|", tableJoiner)));
    }
  }

  private void addSectionHeader(State state, int indent, Background background) {
    addSectionHeader(state, indent, background.getKeyword(), background.getName());
  }

  private void addSectionHeader(State state, int indent, Examples example) {
    addSectionHeader(state, indent, example.getKeyword(), example.getName());
  }

  private void addSectionHeader(State state, int indent, Feature feature) {
    addSectionHeader(state, indent, feature.getKeyword(), feature.getName());
  }

  private void addSectionHeader(State state, int indent, Scenario scenario) {
    addSectionHeader(state, indent, scenario.getKeyword(), scenario.getName());
  }

  private void addSectionHeader(State state, int indent, Rule rule) {
    addSectionHeader(state, indent, rule.getKeyword(), rule.getName());
  }

  private void addSectionHeader(State state, int indent, String keyword, String name) {
    String header = keyword + ":";
    if (!(name == null || name.isEmpty())) {
      header += " " + name;
    }
    state.getJoiner().add(indent(indent, header));
  }

  /*
  Adapted from <code>io.cucumber.gherkin.GherkinLine#getTableCells</code> in gherkin@19.0.2
   */
  private static String cellEscaping(String s) {
    StringBuilder cellBuilder = new StringBuilder();

    boolean escape = false;
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      if (escape) {
        switch (c) {
          case '|':
            cellBuilder.append("\\\\");
            cellBuilder.append("\\|");
            break;
          case '\n':
            cellBuilder.append("\\\\");
            cellBuilder.append("\n");
            break;
          case 'n':
            cellBuilder.append("\\\\");
            cellBuilder.append("n");
            break;
          case '\\':
            cellBuilder.append("\\\\");
            cellBuilder.append("\\\\");
            break;
          default:
            cellBuilder.append("\\");
            cellBuilder.appendCodePoint(c);
            break;
        }
        escape = false;
      } else {
        switch (c) {
          case '|':
            cellBuilder.append("\\|");
            break;
          case '\\':
            escape = true;
            break;
          default:
            cellBuilder.appendCodePoint(c);
            break;
        }
      }
    }
    if (escape) {
      // Invalid escape. We"ll just ignore it.
      cellBuilder.append("\\\\");
    }
    return (cellBuilder.toString().replace("\n", "\\n"));
  }

  private static List<Integer> getMaximumLength(List<TableRow> rows) {
    if (rows.isEmpty()) {
      return Collections.emptyList();
    }

    List<Integer> columnPadding = new ArrayList<>();

    for (int i = 0; i < rows.size(); i++) {
      TableRow row = rows.get(i);
      if (row == null) {
        continue;
      }
      for (int c = 0; c < row.getCells().size(); c++) {
        TableCell cell = row.getCells().get(c);
        if (i == 0) {
          columnPadding.add(0);
        }
        int current = columnPadding.get(c);
        columnPadding.set(c, Math.max(current, cellEscaping(cell.getValue()).length()));
      }
    }
    return columnPadding;
  }

  private static DataTable fromExamples(Examples example) {
    List<TableRow> examplesRows = new ArrayList<>();
    examplesRows.add(example.getTableHeader());
    examplesRows.addAll(example.getTableBody());
    return new DataTable(example.getLocation(), examplesRows);
  }

  private static String transformFormatted(String s) {
    String transformed =
        s.trim().replaceAll("\n +\n", "\n\n").replaceAll(" +\n", "\n").replace("\n\n\n", "\n\n");

    if (transformed.isEmpty()) {
      return transformed;
    }
    return transformed + "\n";
  }
}
