package me.jvt.cucumber.gherkinformatter;

class StringPadder {
  private StringPadder() {
    // utility class
  }

  public static String rightPad(String s, int size) {
    int padding = size - s.length();
    if (padding <= 0) {
      return s;
    }

    StringBuilder builder = new StringBuilder(s);
    for (int i = 0; i < padding; i++) {
      builder.append(" ");
    }
    return builder.toString();
  }
}
