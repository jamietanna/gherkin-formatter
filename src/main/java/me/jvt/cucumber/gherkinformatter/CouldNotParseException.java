package me.jvt.cucumber.gherkinformatter;

import io.cucumber.messages.types.ParseError;

/** An exception to note that the Gherkin feature file could not be parsed for some reason. */
public class CouldNotParseException extends RuntimeException {
  CouldNotParseException(ParseError parseError) {
    super(
        String.format(
            "Could not parse %s:%s", parseError.getSource().getUri(), parseError.getMessage()));
  }
}
