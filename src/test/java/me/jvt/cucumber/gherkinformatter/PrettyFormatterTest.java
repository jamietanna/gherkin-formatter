package me.jvt.cucumber.gherkinformatter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import io.cucumber.gherkin.Gherkin;
import io.cucumber.messages.IdGenerator;
import io.cucumber.messages.types.Envelope;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class PrettyFormatterTest {

  @Nested
  class Spacing extends AbstractTestCases {

    @Override
    int indentInSpaces() {
      return 4;
    }

    @Override
    String filename() {
      return "minimal.feature";
    }

    @Override
    String inputFilename(String filename) {
      return "/input/good/minimal.feature";
    }

    @Test
    void canBeOverridden() {
      String expected = readFile(outputFilename("custom_spacing_minimal.feature"));

      String actual = prettyPrint();

      assertThat(actual).isEqualTo(expected);
    }

    @Test
    void hasDefaults() {
      formatter = new PrettyFormatter();

      String expected = readFile(outputFilename("minimal.feature"));

      String actual = prettyPrint();

      assertThat(actual).isEqualTo(expected);
    }
  }

  @Nested
  class HappyPath {

    @Nested
    class Background extends GoodTest {
      public Background() {
        super("background.feature");
      }
    }

    @Nested
    class ComplexBackground extends GoodTest {
      public ComplexBackground() {
        super("complex_background.feature");
      }
    }

    @Nested
    class Datatables extends GoodTest {
      public Datatables() {
        super("datatables.feature");
      }
    }

    @Nested
    class DatatablesWithComment extends GoodTest {
      public DatatablesWithComment() {
        super("datatables_with_comment.feature");
      }
    }

    @Nested
    class DatatablesWithLongColumns extends GoodTest {
      public DatatablesWithLongColumns() {
        super("datatables_with_long_columns.feature");
      }
    }

    @Nested
    class DatatablesWithNewLines extends GoodTest {
      public DatatablesWithNewLines() {
        super("datatables_with_new_lines.feature");
      }
    }

    @Nested
    class Descriptions extends GoodTest {
      public Descriptions() {
        super("descriptions.feature");
      }
    }

    @Nested
    class Docstrings extends GoodTest {
      public Docstrings() {
        super("docstrings.feature");
      }
    }

    @Nested
    class Empty extends GoodTest {
      public Empty() {
        super("empty.feature");
      }
    }

    @Nested
    class EscapedPipes extends GoodTest {
      public EscapedPipes() {
        super("escaped_pipes.feature");
      }
    }

    @Nested
    class ExampleTokenMultiple extends GoodTest {
      public ExampleTokenMultiple() {
        super("example_token_multiple.feature");
      }
    }

    @Nested
    class ExampleTokensEverywhere extends GoodTest {
      public ExampleTokensEverywhere() {
        super("example_tokens_everywhere.feature");
      }
    }

    @Nested
    class I18nEmoji extends GoodTest {
      public I18nEmoji() {
        super("i18n_emoji.feature");
      }
    }

    @Nested
    class I18nEn extends GoodTest {
      public I18nEn() {
        super("i18n_en.feature");
      }
    }

    @Nested
    class I18nFr extends GoodTest {
      public I18nFr() {
        super("i18n_fr.feature");
      }
    }

    @Nested
    class I18nNo extends GoodTest {
      public I18nNo() {
        super("i18n_no.feature");
      }
    }

    @Nested
    class IncompleteBackground1 extends GoodTest {
      public IncompleteBackground1() {
        super("incomplete_background_1.feature");
      }
    }

    @Nested
    class IncompleteBackground2 extends GoodTest {
      public IncompleteBackground2() {
        super("incomplete_background_2.feature");
      }
    }

    @Nested
    class IncompleteFeature1 extends GoodTest {
      public IncompleteFeature1() {
        super("incomplete_feature_1.feature");
      }
    }

    @Nested
    class IncompleteFeature2 extends GoodTest {
      public IncompleteFeature2() {
        super("incomplete_feature_2.feature");
      }
    }

    @Nested
    class IncompleteFeature3 extends GoodTest {
      public IncompleteFeature3() {
        super("incomplete_feature_3.feature");
      }
    }

    @Nested
    class IncompleteFeature4 extends GoodTest {
      public IncompleteFeature4() {
        super("incomplete_feature_4.feature");
      }
    }

    @Nested
    class IncompleteScenario extends GoodTest {
      public IncompleteScenario() {
        super("incomplete_scenario.feature");
      }
    }

    @Nested
    class IncompleteScenarioOutline extends GoodTest {
      public IncompleteScenarioOutline() {
        super("incomplete_scenario_outline.feature");
      }
    }

    @Nested
    class Language extends GoodTest {
      public Language() {
        super("language.feature");
      }
    }

    @Nested
    class LongTagLine extends GoodTest {

      public LongTagLine() {
        super("long_tag_line.feature");
      }
    }

    @Nested
    class Minimal extends GoodTest {
      public Minimal() {
        super("minimal.feature");
      }
    }

    @Nested
    class MinimalExample extends GoodTest {
      public MinimalExample() {
        super("minimal-example.feature");
      }
    }

    @Nested
    class MinimalWithComment extends GoodTest {
      public MinimalWithComment() {
        super("minimal_with_comment.feature");
      }
    }

    @Nested
    class MinimalWithInconsistentSpacing extends GoodTest {
      public MinimalWithInconsistentSpacing() {
        super("minimal_with_inconsistent_spacing.feature");
      }
    }

    @Nested
    class MinimalWithOddAndEvenSpacing extends GoodTest {
      public MinimalWithOddAndEvenSpacing() {
        super("minimal_with_odd_and_even_spacing.feature");
      }
    }

    @Nested
    class PaddedExample extends GoodTest {
      public PaddedExample() {
        super("padded_example.feature");
      }
    }

    @Nested
    class ReadmeExample extends GoodTest {

      public ReadmeExample() {
        super("readme_example.feature");
      }
    }

    @Nested
    class Rule extends GoodTest {

      public Rule() {
        super("rule.feature");
      }
    }

    @Nested
    class RuleWithTag extends GoodTest {

      public RuleWithTag() {
        super("rule_with_tag.feature");
      }
    }

    @Nested
    class RuleWithoutNameAndDescription extends GoodTest {

      public RuleWithoutNameAndDescription() {
        super("rule_without_name_and_description.feature");
      }
    }

    @Nested
    class ScenarioOutline extends GoodTest {

      public ScenarioOutline() {
        super("scenario_outline.feature");
      }
    }

    @Nested
    class ScenarioOutlineNoNewline extends GoodTest {

      public ScenarioOutlineNoNewline() {
        super("scenario_outline_no_newline.feature");
      }
    }

    @Nested
    class ScenarioOutlineWithDocstring extends GoodTest {

      public ScenarioOutlineWithDocstring() {
        super("scenario_outline_with_docstring.feature");
      }
    }

    @Nested
    class ScenarioOutlinesWithTags extends GoodTest {

      public ScenarioOutlinesWithTags() {
        super("scenario_outlines_with_tags.feature");
      }
    }

    @Nested
    class SeveralExamples extends GoodTest {

      public SeveralExamples() {
        super("several_examples.feature");
      }
    }

    @Nested
    class SpacesInLanguage extends GoodTest {

      public SpacesInLanguage() {
        super("spaces_in_language.feature");
      }
    }

    @Nested
    class TaggedFeatureWithScenarioOutline extends GoodTest {

      public TaggedFeatureWithScenarioOutline() {
        super("tagged_feature_with_scenario_outline.feature");
      }
    }

    @Nested
    class Tags extends GoodTest {

      public Tags() {
        super("tags.feature");
      }
    }

    @Nested
    class TrailingComments extends GoodTest {

      public TrailingComments() {
        super("trailing_comments.feature");
      }
    }

    @Nested
    class VeryLong extends GoodTest {

      public VeryLong() {
        super("very_long.feature");
      }
    }

    abstract class GoodTest extends AbstractTestCases {

      protected final String filename;

      public GoodTest(String filename) {
        this.filename = filename;
      }

      @Override
      String filename() {
        return this.filename;
      }

      String inputFilename(String filename) {
        return "/input/good/" + filename;
      }

      @Test
      @SuppressWarnings("unused")
      void isCorrectlyFormatted() {
        String expected = readFile(outputFilename(filename()));

        String actual = prettyPrint();

        assertThat(actual).isEqualTo(expected);
      }

      @Test
      @SuppressWarnings("unused")
      void isCorrectlyFormattedIgnoringWhitespace() {
        String expected = readFile(outputFilename(filename()));

        String actual = prettyPrint();

        assertThat(actual).isEqualToIgnoringWhitespace(expected);
      }
    }
  }

  @Nested
  class SadPath {
    @Nested
    class InconsistentCellCount extends ParserErrorTest {

      public InconsistentCellCount() {
        super("inconsistent_cell_count.feature");
      }
    }

    @Nested
    class InvalidLanguage extends ParserErrorTest {

      public InvalidLanguage() {
        super("invalid_language.feature");
      }
    }

    @Nested
    class MultipleParserErrors extends ParserErrorTest {

      public MultipleParserErrors() {
        super("multiple_parser_errors.feature");
      }
    }

    @Nested
    class NotGherkin extends ParserErrorTest {

      public NotGherkin() {
        super("not_gherkin.feature");
      }
    }

    @Nested
    class SingleParserError extends ParserErrorTest {

      public SingleParserError() {
        super("single_parser_error.feature");
      }
    }

    @Nested
    class UnexpectedEof extends ParserErrorTest {

      public UnexpectedEof() {
        super("unexpected_eof.feature");
      }
    }

    @Nested
    class WhitespaceInTags extends ParserErrorTest {

      public WhitespaceInTags() {
        super("whitespace_in_tags.feature");
      }
    }

    class ParserErrorTest extends BadTest {

      public ParserErrorTest(String filename) {
        super(filename);
      }

      @Test
      @SuppressWarnings("unused")
      void throwsExceptionWhenPoorlyFormatted() {
        assertThatThrownBy(this::prettyPrint).isInstanceOf(CouldNotParseException.class);
      }
    }

    class BadTest extends AbstractTestCases {

      protected final String filename;

      public BadTest(String filename) {
        this.filename = filename;
      }

      @Override
      String filename() {
        return this.filename;
      }

      String inputFilename(String filename) {
        return "/input/bad/" + filename;
      }
    }
  }

  @Nested
  class PrettyFormatterMethods extends AbstractTestCases {

    @Nested
    class FormatAsString {

      @Test
      void isPublic() {
        String expected = readFile(outputFilename(filename()));

        String actual = prettyPrint();

        assertThat(actual).isEqualToIgnoringWhitespace(expected);
      }
    }

    @Nested
    class FormatAsEnvelopes {

      @Test
      void isPublic() {
        String src = readFile(inputFilename(filename()));
        List<Envelope> envelopes =
            Gherkin.fromSources(
                    Collections.singletonList(Gherkin.makeSourceEnvelope(src, "stdin")),
                    false,
                    true,
                    false,
                    new IdGenerator.Incrementing())
                .collect(Collectors.toList());
        String expected = readFile(outputFilename(filename()));

        String actual = formatter.format(envelopes);

        assertThat(actual).isEqualToIgnoringWhitespace(expected);
      }

      @Test
      void formatAsEnvelopesRequiresAtLeastOneElementInList() {
        assertThatThrownBy(() -> formatter.format(Collections.emptyList()))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("The `envelopes` was empty");
      }

      @Test
      void formatAsEnvelopesRequiresGherkinDocumentToNotBeNull() {
        assertThatThrownBy(() -> formatter.format(Collections.singletonList(new Envelope())))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("The `envelopes` must contain a `GherkinDocument` in the 0th element");
      }
    }

    @Override
    String filename() {
      return "background.feature";
    }

    @Override
    String inputFilename(String filename) {
      return "/input/good/" + filename;
    }
  }

  abstract static class AbstractTestCases {
    protected PrettyFormatter formatter = new PrettyFormatter(indentInSpaces());

    int indentInSpaces() {
      return 2;
    }

    abstract String filename();

    abstract String inputFilename(String filename);

    String outputFilename(String filename) {
      return "/output/" + filename;
    }

    String readFile(String filename) {
      try {
        URL resource = this.getClass().getResource(filename);
        URI uri = Objects.requireNonNull(resource).toURI();
        List<String> lines = Files.readAllLines(Paths.get(uri), Charset.defaultCharset());
        String withNewlines = String.join("\n", lines);
        if (withNewlines.isEmpty()) {
          return withNewlines;
        }

        return withNewlines + "\n";
      } catch (IOException | URISyntaxException e) {
        throw new IllegalStateException(e);
      }
    }

    String prettyPrint() {
      String filename = inputFilename(filename());
      return formatter.format(readFile(filename));
    }
  }
}
